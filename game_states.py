from enum import Enum


class GameStates(Enum):
    PLAYERS_TURN = 1
    TURN = 2
    PLAYER_DEAD = 3
    INVENTORY_MENU = 4
    EQUIPMENT_MENU = 5
    MAIN_MENU = 6
    LEVEL_UP = 7
    CHARACTER_SCREEN = 8
