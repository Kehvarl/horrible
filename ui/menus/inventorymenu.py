from enum import Enum

import tcod
import tcod.event

from ui.menus.menu import Menu


class InventoryCommands(Enum):
    CANCEL = 0
    DROP = 1
    USE = 2
    CONSUME = 3
    EQUIP = 4


class InventoryMenu(Menu):
    def __init__(self,
                 width=80,
                 height=50,
                 background_image=None,
                 bg_color=tcod.black,
                 fg_color=tcod.white,
                 options_title="",
                 options=None):

        super().__init__(width, height, background_image, bg_color, fg_color)

        self.options_title = options_title
        self.options = options
        self.selected_option = 0
        self.commands = [InventoryCommands.USE, InventoryCommands.DROP]
        self.selected_command = 0

    def process(self):
        for event in tcod.event.get():
            if event.type == "QUIT":
                raise SystemExit()
            elif event.type == "KEYDOWN":
                if event.sym in [tcod.event.K_UP, ord('w'), ord('k')]:
                    self.selected_option = (self.selected_option - 1) % (len(self.options))
                elif event.sym in [tcod.event.K_DOWN, ord('s'), ord('j')]:
                    self.selected_option = (self.selected_option + 1) % (len(self.options))
                elif event.sym in [tcod.event.K_LEFT, ord('a'), ord('h')]:
                    self.selected_command = (self.selected_command - 1) % (len(self.commands))
                elif event.sym in [tcod.event.K_RIGHT, ord('d'), ord('l')]:
                    self.selected_command = (self.selected_command + 1) % (len(self.commands))
                elif event.sym in [tcod.event.K_ESCAPE]:
                    return InventoryCommands.CANCEL, None
                elif event.sym in [ord('p')]:
                    return InventoryCommands.DROP, self.options[self.selected_option][0]
                elif event.sym in [ord('c')]:
                    return InventoryCommands.CONSUME, self.options[self.selected_option][0]
                elif event.sym in [ord('e')]:
                    return InventoryCommands.EQUIP, self.options[self.selected_option][0]
                elif event.sym in [tcod.event.K_RETURN, tcod.event.K_KP_ENTER, 'u']:
                    return self.commands[self.selected_command], self.options[self.selected_option][0]

    def draw_menu(self):
        super().draw_menu()
        self.draw_menu_options(self.options_title, self.options, 15, 10, 20)
        if len(self.options) > 0:
            name = self.options[self.selected_option][1]
            desc = self.options[self.selected_option][2]
            self.draw_selected_option(name, desc, 50, 25, 20)

    def draw_menu_options(self, header, options, x, y, width):

        header_height = tcod.console_get_height_rect(self.console, 0, 0, width, self.console.width, header)
        height = len(options) + header_height

        window = tcod.console_new(width, height)

        tcod.console_set_default_foreground(window, tcod.white)
        tcod.console_print_rect_ex(window, 0, 0, width, height, tcod.BKGND_NONE, tcod.LEFT, header)

        line = header_height
        letter_index = ord('a')
        for code, option_text, _ in options:
            color = tcod.black
            if code == self.options[self.selected_option][0]:
                color = tcod.blue
            self._draw_bar(window, 0, line, width, 1, option_text, color)
            line += 1
            letter_index += 1

        tcod.console_blit(window, 0, 0, width, height, self.console, x, y, 1.0, 0.7)

    def draw_selected_option(self, header, content, x, y, width):
        header_height = tcod.console_get_height_rect(self.console, 0, 0, width, 1, header)
        content_height = tcod.console_get_height_rect(self.console, 0, 0, width, 1, content)
        height = content_height + header_height + 1

        window = tcod.console_new(width, height)

        tcod.console_print_rect_ex(window, 0, 0, width, height, tcod.BKGND_NONE, tcod.LEFT, header)
        window.print_rect(0, header_height, width, content_height, content)

        self._draw_bar(window, 0, height - 1, width // 2, 1, "Use",
                       tcod.blue if self.commands[self.selected_command] == InventoryCommands.USE else tcod.black)
        self._draw_bar(window, width // 2, height - 1, width // 2, 1, "Drop",
                       tcod.blue if self.commands[self.selected_command] == InventoryCommands.DROP else tcod.black)

        tcod.console_blit(window, 0, 0, width, height, self.console, x, y, 1.0, 0.7)
