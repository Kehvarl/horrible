import tcod
import tcod.console
import tcod.event


class Menu:
    def __init__(self,
                 width=80,
                 height=50,
                 background_image=None,
                 bg_color=tcod.black,
                 fg_color=tcod.white):

        self.console = tcod.console.Console(width, height, 'F')

        if background_image is not None:
            self.background_image = tcod.image_load(background_image)
        else:
            self.background_image = None
        self.bg_color = bg_color
        self.fg_color = fg_color

    def blit(self, target_console):
        self.console.blit(target_console)

    def process(self):
        pass

    def draw_menu(self):
        if self.background_image is not None:
            tcod.image_blit_2x(self.background_image, self.console, 0, 0)
        elif self.bg_color:
            self.console.clear(bg=self.bg_color)

    @staticmethod
    def _draw_bar(con, x, y, width, height, text, bg_color=tcod.black, fg_color=tcod.white):
        con.draw_rect(x, y, width, height, ch=0, fg=fg_color, bg=bg_color)
        con.print_box(x, y, width, height, text, fg=fg_color, alignment=tcod.CENTER)


class MenuMultiOptions(Menu):
    def __init__(self,
                 width=80,
                 height=50,
                 background_image=None,
                 bg_color=tcod.black,
                 fg_color=tcod.white,
                 options_titles=None,
                 options=None):

        super().__init__(width, height, background_image, bg_color, fg_color)

        self.options_title = options_titles
        self.options = options
        self.option_set = 0
        self.selected_option = 0

    def process(self):
        for event in tcod.event.get():
            if event.type == "QUIT":
                raise SystemExit()
            elif event.type == "KEYDOWN":
                if event.sym in [tcod.event.K_UP, ord('w'), ord('k')]:
                    self.selected_option = (self.selected_option - 1) % (len(self.options[self.option_set]))
                elif event.sym in [tcod.event.K_DOWN, ord('s'), ord('j')]:
                    self.selected_option = (self.selected_option + 1) % (len(self.options[self.option_set]))
                elif event.sym in [tcod.event.K_LEFT, ord('a'), ord('h')]:
                    self.selected_option = 0
                    self.option_set = (self.option_set - 1) % (len(self.options))
                elif event.sym in [tcod.event.K_RIGHT, ord('d'), ord('l')]:
                    self.selected_option = 0
                    self.option_set = (self.option_set + 1) % (len(self.options))
                elif event.sym in [tcod.event.K_RETURN, tcod.event.K_KP_ENTER]:
                    return self.options[self.selected_option][0]

    def draw_menu(self):
        super().draw_menu()
        self.draw_menu_options(self.options_title[0], self.options[0], 7, 25, 20)
        if self.options_title[1]:
            self.draw_menu_options(self.options_title[1], self.options[1], 30, 25, 20)
        if self.options_title[2]:
            self.draw_menu_options(self.options_title[2], self.options[2], 53, 25, 20)

    def draw_menu_options(self, header, options, x, y, width):

        header_height = tcod.console_get_height_rect(self.console, 0, 0, width, self.console.width, header)
        height = len(options) + header_height

        window = tcod.console_new(width, height)

        tcod.console_set_default_foreground(window, tcod.white)
        tcod.console_print_rect_ex(window, 0, 0, width, height, tcod.BKGND_NONE, tcod.LEFT, header)

        line = header_height
        letter_index = ord('a')
        for code, option_text in options:
            color = tcod.black
            if code == self.options[self.option_set][self.selected_option][0]:
                color = tcod.blue
            self._draw_bar(window, 0, line, width, 1, option_text, color)
            line += 1
            letter_index += 1

        tcod.console_blit(window, 0, 0, width, height, self.console, x, y, 1.0, 0.7)
