from enum import Enum

import tcod
import tcod.event

from ui.menus.menu import Menu


class EquipmentCommands(Enum):
    CANCEL = 0
    EQUIP = 1
    UNEQUIP = 2


# 2020-04-13
# Item ---- Slots ---- Item in current slot
#        Equip|Cancel

class EquipmentMenu(Menu):
    # Item:  The item being considered
    # Slots:  The current slots and their current equipment (SLOT, Item)
    def __init__(self,
                 width=80,
                 height=50,
                 background_image=None,
                 bg_color=tcod.black,
                 fg_color=tcod.white,
                 options_title="",
                 item=None,
                 slots=None):

        super().__init__(width, height, background_image, bg_color, fg_color)

        self.options_title = options_title
        self.item = item
        self.slots = slots
        self.selected_slot = 0
        self.commands = [EquipmentCommands.EQUIP, EquipmentCommands.UNEQUIP, EquipmentCommands.CANCEL]
        self.selected_command = 0

    def process(self):
        # Direction Keys Up, Down:  Change selected slot
        # Direction Keys Left, Right: Change command
        # Enter: Selected Command
        # E: Equip into slot
        # R: Remove item in slot
        # ESC: Cancel
        for event in tcod.event.get():
            if event.type == "QUIT":
                raise SystemExit()
            elif event.type == "KEYDOWN":
                if event.sym in [tcod.event.K_UP, ord('w'), ord('k')]:
                    self.selected_slot = (self.selected_slot - 1) % (len(self.slots))
                elif event.sym in [tcod.event.K_DOWN, ord('s'), ord('j')]:
                    self.selected_slot = (self.selected_slot + 1) % (len(self.slots))
                elif event.sym in [tcod.event.K_LEFT, ord('a'), ord('h')]:
                    self.selected_command = (self.selected_command - 1) % (len(self.commands))
                elif event.sym in [tcod.event.K_RIGHT, ord('d'), ord('l')]:
                    self.selected_command = (self.selected_command + 1) % (len(self.commands))
                elif event.sym in [tcod.event.K_ESCAPE]:
                    return EquipmentCommands.CANCEL, None
                elif event.sym in [ord('e')]:
                    return EquipmentCommands.EQUIP, self.slots[self.selected_slot]
                elif event.sym in [ord('r')]:
                    return EquipmentCommands.UNEQUIP, self.slots[self.selected_slot]
                elif event.sym in [tcod.event.K_RETURN, tcod.event.K_KP_ENTER, 'u']:
                    return self.commands[self.selected_command], self.slots[self.selected_slot]

    def draw_menu(self):
        super().draw_menu()
        self.draw_equipment_item(self.item, 15, 25, 20)
        self.draw_menu_options(self.options_title, self.slots, 50, 10, 20)
        self.draw_equipment_item(self.slots[self.selected_slot][1], 75, 25, 20)

    def draw_menu_options(self, header, options, x, y, width):

        header_height = tcod.console_get_height_rect(self.console, 0, 0, width, self.console.width, header)
        height = len(options) + header_height

        window = tcod.console_new(width, height)

        tcod.console_set_default_foreground(window, tcod.white)
        tcod.console_print_rect_ex(window, 0, 0, width, height, tcod.BKGND_NONE, tcod.LEFT, header)

        line = header_height
        letter_index = ord('a')
        for code in options:
            color = tcod.black
            if code == self.slots[self.selected_slot][0]:
                color = tcod.blue
            self._draw_bar(window, 0, line, width, 1, code, color)
            line += 1
            letter_index += 1

        tcod.console_blit(window, 0, 0, width, height, self.console, x, y, 1.0, 0.7)

    def draw_equipment_item(self, item, x, y, width):
        header = item.name
        content = item.description
        header_height = tcod.console_get_height_rect(self.console, 0, 0, width, 1, header)
        content_height = tcod.console_get_height_rect(self.console, 0, 0, width, 1, content)
        height = content_height + header_height + 1

        window = tcod.console_new(width, height)

        tcod.console_print_rect_ex(window, 0, 0, width, height, tcod.BKGND_NONE, tcod.LEFT, header)
        window.print_rect(0, header_height, width, content_height, content)

        self._draw_bar(window, 0, height - 1, width // 2, 1, "Use",
                       tcod.blue if self.commands[self.selected_command] == EquipmentCommands.USE else tcod.black)
        self._draw_bar(window, width // 2, height - 1, width // 2, 1, "Drop",
                       tcod.blue if self.commands[self.selected_command] == EquipmentCommands.DROP else tcod.black)

        tcod.console_blit(window, 0, 0, width, height, self.console, x, y, 1.0, 0.7)
