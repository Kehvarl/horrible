import tcod
from tcod import console


class UI:
    def __init__(self, game, game_title, bg_color=tcod.black):
        self.bg_color = bg_color

        self.map_console = tcod.console.Console(game.level_map.width, game.level_map.height, order='F')

        self.stats_console = tcod.console.Console(20, 40, 'F')

        self.message_console = tcod.console.Console(80, 10, 'F')

        tcod.console_set_custom_font(
            'resources/fonts/dejavu12x12_gs_tc.png',
            tcod.FONT_LAYOUT_TCOD | tcod.FONT_TYPE_GREYSCALE)

        self.root_console = tcod.console_init_root(80, 50,
                                                   game_title,
                                                   renderer=tcod.RENDERER_OPENGL2,
                                                   vsync=True,
                                                   order="F")

    def render_menu(self, menu):
        menu.draw_menu()
        menu.blit(self.root_console)
        tcod.console_flush()

    def render(self, game, fg_color=tcod.white, bg_color=None):
        if bg_color is None:
            bg_color = self.bg_color

        self._draw_map(game)
        self._draw_messages(game, fg_color, bg_color)
        self._draw_stats(game, fg_color, bg_color)

        self.root_console.clear(bg=bg_color)
        self.map_console.blit(self.root_console, 20, 0)
        self.stats_console.blit(self.root_console, 0, 0)
        self.message_console.blit(self.root_console, 0, 40)
        tcod.console_flush()

    def _draw_map(self, game):
        game.level_map.fill_buffer(self.map_console, game.current_level)

    def _draw_stats(self, game, fg_color, bg_color=None):
        if bg_color is None:
            bg_color = self.bg_color
        self.stats_console.clear(fg=fg_color, bg=bg_color)
        self.stats_console.draw_frame(0, 0, 20, 40, "Player", True, fg=fg_color, bg=bg_color)

        player = game.player_entity

        self._draw_bar(self.stats_console, 1, 1, 18, 1, "HP",
                       player.component_fighter.max_hp, player.component_fighter.hp)

        self._draw_bar(self.stats_console, 1, 3, 18, 1, "XP",
                       100, player.component_fighter.xp,
                       fg_color, tcod.blue, tcod.light_blue, tcod.grey, tcod.black)

        y = 5
        if player.component_equipment:
            for slot in player.component_equipment.slots:
                if player.component_equipment.equipment.get(slot):
                    eq = player.component_equipment.equipment[slot].component_equipable
                    y += self.stats_console.print_box(1, y, 18, 2, str(eq), tcod.grey)

    @staticmethod
    def _draw_bar(con, x, y, width, height, text, max_value, actual_value,
                  fg_color=tcod.white,
                  color_100=tcod.green, color_75=tcod.orange,
                  color_50=tcod.dark_yellow, color_25=tcod.red):

        text = "{} {}/{}".format(text, actual_value, max_value)
        ratio = (actual_value / max_value)
        color = color_100 if ratio > 0.75 else color_75 if ratio >= 0.50 else color_50 if ratio > 0.25 else color_25
        line = int(width * ratio)
        con.draw_rect(x, y, line, height, ch=0, fg=fg_color, bg=color)
        con.print_box(x, y, width, height, text, fg=fg_color, alignment=tcod.CENTER)

    def _draw_messages(self, game, fg_color, bg_color=None):
        if bg_color is None:
            bg_color = self.bg_color
        self.message_console.clear(fg=fg_color, bg=bg_color)
        self.message_console.draw_frame(0, 0, 80, 10, "Messages", True, fg=fg_color, bg=bg_color)
        y = 1
        for m in filter(lambda lm: len(lm.text) > 0, game.level_map.player_entity.messages):
            self.message_console.print(1, y, m.text, m.color)
            y += 1
