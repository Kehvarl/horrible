class Actor:
    def __init__(self, energy, energy_gain):
        """

        :param energy: Max Energy Level
        :param energy_gain: Energy Gained per turn
        """
        self.max_energy = energy
        self.energy = energy
        self.energy_gain = energy_gain
        self.owner = None

    def gain_energy(self):
        self.energy = min(self.energy + self.energy_gain, self.max_energy)
