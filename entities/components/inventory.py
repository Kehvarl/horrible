from random import choice

import tcod

from entities.components.consumable import Consumable
from entities.components.equipable import Equipable
from messaging.game_message import Message


class Inventory:
    def __init__(self, capacity=0):
        self.capacity = capacity
        self.value = 0
        self.power_bonus = 0
        self.defense_bonus = 0
        self.stored_items = []
        self.active_items = []
        self.owner = None

    def add(self, item):
        if len(self.stored_items) >= self.capacity:
            return False
        self.stored_items.append(item)
        self.stored_items.sort()
        return True

    def use(self, item):
        result = []
        if item in self.stored_items:
            if item.duration > 0:
                self.active_items.append(item)
                self.active_items.sort()
            self.stored_items.remove(item)
            self.power_bonus += item.power_bonus
            self.defense_bonus += item.defense_bonus
            result.append(Message(self.owner.pos, "{} has used {}".format(self.owner, item),
                                  effect={'HP': item.hp_bonus}, color=tcod.white))
        return result

    def drop(self, item, level_map):
        result = []
        if item in self.stored_items:
            self.stored_items.remove(item)
            item.x, item.y = self.owner.pos
            level_map.entities.append(item)
            result.append(Message(self.owner.pos, "{} dropped a {} on the floor".format(self.owner, item)))

        return result

    def pick_up(self, level_map):
        result = []
        items = level_map.get_entities_at_location(self.owner.x, self.owner.y)
        for item in items:
            if item == self.owner:
                continue
            if self.add(item):
                level_map.entities.remove(item)
                result.append(Message(self.owner.pos, "{} picked up {}".format(self.owner, item)))
                break

        return result

    @property
    def stored_items_options(self):
        result = []
        for item in self.stored_items:
            result.append((item, item.name, item.description))
        return result

    @property
    def consumables(self):
        result = []
        for item in self.stored_items:
            if isinstance(item, Consumable):
                result.append(item)
        return result

    @property
    def equipables(self):
        result = []
        for item in self.stored_items:
            if isinstance(item, Equipable):
                result.append(item)
        return result

    def steal(self, item=None):
        if item is None:
            item = choice(self.consumables)
        if item in self.stored_items:
            self.stored_items.remove(item)
            return item
        return False
