class Equipment:
    def __init__(self, slots: set = None):
        self.equipment = {}

        if slots is None:
            self.slots = set()
        else:
            self.slots = slots

        self.owner = None

    def equip(self, item, slot):
        un_equip = item
        if item.component_equipable and (
                item.component_equipable.slots is None or slot in item.component_equipable.slots):
            if slot in self.slots:
                if self.equipment.get(slot):
                    un_equip = self.equipment[slot]
                self.equipment[slot] = item

        return un_equip

    @property
    def slot_items(self):
        output = []
        for slot in self.slots:
            output.append((slot, self.equipment.get(slot, None)))
        return output

    @property
    def max_hp(self):
        _hp = 0
        for slot in self.slots:
            if self.equipment.get(slot):
                _hp += self.equipment[slot].component_equipable.max_hp_bonus

        return _hp

    @property
    def power(self):
        _power = 0
        for slot in self.slots:
            if self.equipment.get(slot):
                _power += self.equipment[slot].component_equipable.power_bonus

        return _power

    @property
    def defense(self):
        _defense = 0
        for slot in self.slots:
            if self.equipment.get(slot):
                _defense += self.equipment[slot].component_equipable.defense_bonus

        return _defense
