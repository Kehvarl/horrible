
class Equipable:
    def __init__(self, slots=None, max_hp_bonus=0, power_bonus=0, defense_bonus=0):
        self.slots = slots
        self.max_hp_bonus = max_hp_bonus
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus

        self.owner = None

    def allowed_in_slot(self, slot):
        if self.slots is None or len(self.slots) == 0:
            return True
        if "ANY" in self.slots:
            return True
        if slot in self.slots:
            return True
        if slot in ['RIGHT_HAND', 'LEFT_HAND'] and 'HAND' in self.slots:
            return True

    def __repr__(self):
        return "{}:\n H:{}, P:{}, D:{}".format(self.owner.name,
                                               self.max_hp_bonus,
                                               self.power_bonus,
                                               self.defense_bonus)
