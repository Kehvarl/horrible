import numpy as np
import tcod.map


class Viewer:
    def __init__(self, radius=10, width=10, height=10):
        self.owner = None

        self.fov = np.full((width, height), False, order='F')
        self.explored = np.full((100, width, height), False, order='F')

        self.algorithm = tcod.FOV_SHADOW
        self.fov_radius = radius

    def update_fov(self, level_map):
        self.fov = tcod.map.compute_fov(level_map.transparent, (self.owner.x, self.owner.y),
                                        self.fov_radius,
                                        light_walls=True, algorithm=self.algorithm)
        np.logical_or(self.fov, self.explored[level_map.level], out=self.explored[level_map.level])
