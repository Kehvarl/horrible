import tcod

from entities.components.ai import DeadAI
from entities.components.player_controlled import PlayerControlled
from entities.entity import RenderOrder
from messaging.game_message import Message


class Fighter:
    def __init__(self, hp, defense, power, xp=0):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.base_power = power
        self.xp = xp
        self.owner = None

    @property
    def max_hp(self):
        if self.owner and self.owner.component_equipment:
            bonus = self.owner.component_equipment.max_hp
        else:
            bonus = 0
        return self.base_max_hp + bonus

    @property
    def defense(self):
        if self.owner and self.owner.component_equipment:
            bonus = self.owner.component_equipment.defense
        else:
            bonus = 0
        return self.base_defense + bonus

    @property
    def power(self):
        if self.owner and self.owner.component_equipment:
            bonus = self.owner.component_equipment.power
        else:
            bonus = 0
        return self.base_power + bonus

    @property
    def can_level_up(self):
        return self.xp > 100

    def take_damage(self, amount):
        messages = []

        self.hp -= amount

        if self.hp <= 0:
            messages.append(Message(self.owner.pos, "{} has died".format(self.owner), tcod.red))
            if self.owner.component_ai and not isinstance(self.owner.component_ai, PlayerControlled):
                self.owner.component_ai.swap_ai(DeadAI(), 0)

            self.owner.render_order = RenderOrder.DEAD_ENTITY
            self.owner.color = (240, 0, 0)

        return messages

    def heal(self, amount):
        self.hp += amount

        if self.hp > self.max_hp:
            self.hp = self.max_hp

    def attack(self, target):
        messages = []

        damage = self.power - target.component_fighter.defense
        if damage > 0:
            messages.append(
                Message(self.owner.pos, "{} attacks {} for {}".format(self.owner, target, damage), tcod.white))
            messages.extend(target.component_fighter.take_damage(damage))

            if target.component_fighter.hp <= 0 and target.component_treasure:
                self.xp += target.component_treasure.xp
                target.component_treasure.xp = 0

        return messages
