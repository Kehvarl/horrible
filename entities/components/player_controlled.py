import tcod.event

from entities.components.ai import AI
from messaging.game_message import Message


class PlayerControlled(AI):
    def __init__(self):
        super().__init__()

    def ai_turn(self, level_map):

        dx = 0
        dy = 0
        messages = []
        end_player_turn = False
        for event in tcod.event.get():
            if event.type == "QUIT":
                #  tcod.sys_save_screenshot()
                raise SystemExit()
            elif event.type == "KEYDOWN":
                force_attack = (event.mod & tcod.event.KMOD_CTRL)
                force_peace = (event.mod & tcod.event.KMOD_ALT)
                px, py = level_map.player_entity.pos

                if event.sym in [tcod.event.K_UP, ord('w'), ord('k')]:
                    py -= 1
                    dy = -1
                    end_player_turn = True
                elif event.sym in [tcod.event.K_DOWN, ord('s'), ord('j')]:
                    py += 1
                    dy = 1
                    end_player_turn = True
                elif event.sym in [tcod.event.K_LEFT, ord('a'), ord('h')]:
                    px -= 1
                    dx = -1
                    end_player_turn = True
                elif event.sym in [tcod.event.K_RIGHT, ord('d'), ord('l')]:
                    px += 1
                    dx = 1
                    end_player_turn = True
                elif event.sym in [ord('t')]:
                    if level_map.player_entity.component_inventory:
                        messages.extend(level_map.player_entity.component_inventory.pick_up(level_map))
                        end_player_turn = True
                elif event.sym in [ord('b')]:
                    messages.append(Message(self.owner.pos, "", effect="DROP"))
                elif event.sym in [ord('v')]:
                    if level_map.tiles[px, py] == 'E':
                        messages.append(Message(level_map.player_entity.pos,
                                                'Activating Elevator',
                                                tcod.white, 'FLOOR_UP'))
                        end_player_turn = True
                elif event.sym in [ord('r')]:
                    end_player_turn = True
                elif event.sym in [ord('`')]:
                    tcod.sys_save_screenshot()

                e = level_map.get_blocking_entities_at_location(px, py)
                if end_player_turn and (not force_peace) and \
                        e and e.component_fighter and e.component_fighter.hp > 0 and \
                        (force_attack or e.faction not in level_map.player_entity.ally_factions):
                    messages.extend(level_map.player_entity.component_fighter.attack(e))
                level_map.move(level_map.player_entity, dx=dx, dy=dy)

                if not end_player_turn:
                    return False

                return messages

            elif event.type == "MOUSEBUTTONDOWN":
                pass
            elif event.type == "MOUSEMOTION":
                pass

        return False
