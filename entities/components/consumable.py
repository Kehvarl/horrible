class Consumable:
    def __init__(self, hp_bonus, power_bonus, defense_bonus, special_effects=None, duration=0):
        self.hp_bonus = hp_bonus
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus
        if special_effects is None:
            self.special_effects = []
        else:
            self.special_effects = special_effects
        self.duration = duration

        self.owner = None

    def __repr__(self):
        return "{}: {}".format(self.owner.name, self.owner.description)
