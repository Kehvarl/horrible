from random import randint

import tcod.path

from entities.entity import RenderOrder
from world.level_map import LevelMap


class AI:
    def __init__(self):
        self.owner = None
        self.previous_ai = None
        self.revert_countdown = 0

    def ai_turn(self, level_map):
        if self.previous_ai:
            self.revert_countdown -= 1
        if self.revert_countdown < 0:
            self.swap_ai(self.previous_ai, None)
            self.owner.component_ai.ai_turn(level_map)

    def swap_ai(self, new_ai, revert_turn_count=None):
        if revert_turn_count is not None:
            self.revert_countdown = revert_turn_count
            self.previous_ai = self.owner.component_ai
        else:
            self.previous_ai = None
        self.owner.component_ai = new_ai
        self.owner.component_ai.owner = self.owner
        if isinstance(new_ai, DeadAI):
            self.owner.render_order = RenderOrder.DEAD_ENTITY
        elif isinstance(self.previous_ai, DeadAI):
            self.owner.render_order = RenderOrder.LIVE_ENTITY


class DeadAI(AI):
    def __init__(self):
        super().__init__()

    def ai_turn(self, _):
        return []


class BasicAI(AI):
    def __init__(self):
        super().__init__()

    def ai_turn(self, level_map):
        """
        If adjacent enemy, attack it.
        Otherwise move randomly

        :param LevelMap level_map:
        """
        super().ai_turn(level_map)
        messages = []
        monster = self.owner
        adj = level_map.adjacent_entities(monster)
        target = None
        for e in adj:
            if e.faction not in monster.ally_factions and \
                    e.component_fighter and \
                    e.component_fighter.hp > 0:
                target = e
                break

        if target:
            attack_results = monster.component_fighter.attack(target)
            messages.extend(attack_results)
        else:  # random walk.  Convert to "Explore" at some point
            random_x = self.owner.x + randint(0, 2) - 1
            random_y = self.owner.y + randint(0, 2) - 1
            level_map.move_towards(monster, random_x, random_y)

        return messages


class GuardianAI(AI):
    def __init__(self, room=None):
        super().__init__()
        if room:
            self.room = room

    def ai_turn(self, level_map):
        """
        Guardian AI Turn
        If any enemy in room, move towards and attack
        Otherwise move randomly within the room.

        :param LevelMap level_map:
        """
        super().ai_turn(level_map)
        messages = []
        monster = self.owner
        adj = level_map.adjacent_entities(monster)
        target = None
        for e in adj:
            if e.faction not in monster.ally_factions and \
                    e.component_fighter and \
                    e.component_fighter.hp > 0:
                target = e
                break

        if not target:
            in_room = level_map.entities_in_room(self.room, self.owner)
            for e in in_room:
                if e.faction not in monster.ally_factions and \
                        e.component_fighter and \
                        e.component_fighter.hp > 0:
                    target = e
                    break

        if target:
            attack_results = monster.component_fighter.attack(target)
            messages.extend(attack_results)
        else:  # random walk within room.  Convert to "Explore" at some point
            rx, ry = self.room.random_point_in_room()

            level_map.move_towards(monster, rx, ry)

        return messages


class FollowAI(AI):
    def __init__(self):
        """

        """
        super().__init__()
        self.following = None
        self.default_follow_turns = 10
        self.follow_turns = 10
        self.follow_forever = False

    def ai_turn(self, level_map):
        """
        Follower AI Turn
        Follow allies around the map
        :param LevelMap level_map:
        """
        super().ai_turn(level_map)

        monster = self.owner

        if not self.follow_forever:
            self.follow_turns -= 1

        if self.follow_turns < 0:
            self.following = None

        messages = []
        if self.following:
            if self.following not in level_map.entities:
                self.following = None

        if not self.following:
            self.following = self.get_nearest(level_map.entities)
            self.follow_turns = self.default_follow_turns

        target = self.following

        if target and (monster.component_viewer.fov[monster.x, monster.y]):
            if monster.distance_to(target) >= 3:
                path = tcod.path.AStar(level_map.walkable)
                step = path.get_path(monster.x, monster.y, target.x, target.y)[0]
                sx, sy = step
                level_map.move(monster, sx - monster.x, sy - monster.y)
            else:
                random_x = monster.x + randint(0, 2) - 1
                random_y = monster.y + randint(0, 2) - 1
                level_map.move_towards(monster, random_x, random_y)
        else:
            random_x = monster.x + randint(0, 2) - 1
            random_y = monster.y + randint(0, 2) - 1
            level_map.move_towards(monster, random_x, random_y)

        return messages

    def get_nearest(self, entities):
        nearest = self.owner.distance_to(entities[0])
        entity = entities[0]
        for e in entities:
            if e == self.owner:
                continue
            if self.owner.distance_to(e) < nearest:
                nearest = self.owner.distance_to(e)
                entity = e
        return entity
