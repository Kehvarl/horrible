import math
from enum import Enum

from messaging.game_message import MessageList


class RenderOrder(Enum):
    PLAYER = 1
    LIVE_ENTITY = 2
    ITEM = 3
    DEAD_ENTITY = 8


class Entity:
    def __init__(self, x, y, symbol, color, name, faction, blocks,
                 description=None,
                 special_effects=None,
                 component_equipable=None, component_consumable=None,
                 component_treasure=None,
                 component_ai=None, component_actor=None, component_equipment=None,
                 component_fighter=None, component_inventory=None,
                 component_viewer=None, render_order=RenderOrder.LIVE_ENTITY):
        """

        :param x:
        :param y:
        :param symbol:
        :param color:
        :param name:
        :param faction:
        :param blocks:
        :param description:
        :param special_effects:
        :param component_equipable:
        :param component_consumable:
        :param component_treasure:
        :param component_ai:
        :param component_actor:
        :param component_equipment:
        :param component_fighter:
        :param component_inventory:
        :param component_viewer:
        :param render_order:
        """

        self.x = x
        self.y = y
        self.symbol = symbol
        self.color = color
        self.name = name
        if description is None:
            self.description = self.name
        else:
            self.description = description

        self.faction = faction
        self.ally_factions = {faction, "Neutral"}
        self.blocks = blocks
        self.render_order = render_order
        self.messages = MessageList(max_lines=8)
        self.special_effects = []
        if special_effects is not None:
            self.special_effects = special_effects

        self.component_equipable = component_equipable
        if self.component_equipable:
            self.component_equipable.owner = self

        self.component_consumable = component_consumable
        if self.component_consumable:
            self.component_consumable.owner = self

        self.component_treasure = component_treasure

        self.component_ai = component_ai
        if self.component_ai:
            self.component_ai.owner = self

        self.component_actor = component_actor
        if self.component_actor:
            self.component_actor.owner = self

        self.component_equipment = component_equipment
        if self.component_equipment:
            self.component_equipment.owner = self

        self.component_fighter = component_fighter
        if self.component_fighter:
            self.component_fighter.owner = self

        self.component_inventory = component_inventory
        if self.component_inventory:
            self.component_inventory.owner = self

        self.component_viewer = component_viewer
        if self.component_viewer:
            self.component_viewer.owner = self

    @property
    def pos(self):
        return self.x, self.y

    def distance_to(self, other):
        """
        Calculate the straight-line distance between this Entity and another Entity
        :param Entity other:
        :return int: The distance in tiles
        """
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)

    def distance(self, x, y):
        """
        Calculate the straight-line distance between this entity and a tile on the map
        :param int x: Coordinate of target
        :param int y: Coordinate of target
        :return int: The distance in tiles
        """
        return math.sqrt((x - self.x) ** 2 + (y - self.y) ** 2)

    def take_turn(self, level_map):
        messages = []

        if self.component_viewer:
            self.component_viewer.update_fov(level_map)

        if self.component_actor:
            self.component_actor.gain_energy()

        if self.component_ai:
            result = self.component_ai.ai_turn(level_map)
            if isinstance(result, list):
                messages.extend(result)
            else:
                messages = False

        return messages

    def __lt__(self, other):
        return self.__str__() < other.__str__()

    def __str__(self):
        return self.name

    def __repr__(self):
        return "{} at ({},{})".format(self.name, self.x, self.y)
