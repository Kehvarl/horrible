from entities.components.consumable import Consumable
from entities.components.equipable import Equipable
from entities.components.treasure import Treasure as Tr
from entities.entity import Entity
from loaders.treasure_templates import Treasure


def from_template(template: Treasure, level_map, room=None):
    equipment_component = None
    treasure_component = None
    consumable_component = None

    if room is None:
        room = level_map.random_room()
    x, y = room.random_point_in_room()

    if "equipment" in template.components:
        slot, hp, power, defense = template.components["equipment"]
        equipment_component = Equipable(slots=[slot], max_hp_bonus=hp, power_bonus=power, defense_bonus=defense)

    if "treasure" in template.components:
        value, xp = template.components["treasure"]
        treasure_component = Tr(int(value), int(xp))

    if "consumable" in template.components:
        hp, power, defense, duration, special = template.components["consumable"]
        consumable_component = Consumable(hp, power, defense, duration=duration)

    return Entity(x, y, template.symbol, template.color, template.name, "Neutral", blocks=False,
                  description=template.description,
                  component_equipment=equipment_component,
                  component_treasure=treasure_component,
                  component_consumable=consumable_component)
