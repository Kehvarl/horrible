from random import choice, randint

import tcod

# from world.level_map import LevelMap
import world.map_generator
from entities.components.actor import Actor
from entities.components.ai import FollowAI
from entities.components.equipable import Equipable
from entities.components.equipment import Equipment
from entities.components.fighter import Fighter
from entities.components.inventory import Inventory
from entities.components.player_controlled import PlayerControlled
from entities.components.viewer import Viewer
from entities.entity import Entity, RenderOrder
from loaders.colors import Color
from loaders.enemy_templates import Enemy
from loaders.level_templates import Layout
from loaders.treasure_templates import Treasure
from messaging.game_message import Message
from world.enemy_generator import from_template
from world.treasure_generator import from_template as treasure_template


class World:
    def __init__(self):
        self.room_templates = ["##..##\n#....#\n..EE..\n..EE..\n#....#\n##..##".split("\n")]
        self.level_templates = Layout.load_all_from_toml_file("resources/layouts.toml")
        self.color_sets = Color.load_all_from_toml_file("resources/colors.toml")
        self.enemy_templates = Enemy.load_all_from_toml_file("resources/enemies.toml")
        self.random_treasure_templates = Treasure.load_all_from_toml_file("resources/treasure.toml")
        self.room_treasure_templates = Treasure.load_all_from_toml_file("resources/treasure_rooms.toml")

        self.level_maps = []
        self.current_level = None
        self.player_entity = None

        self.global_messages = []

        self.new_level()
        self.new_player()
        self.spawn_entities()
        self.spawn_boss_entities()
        self.spawn_treasure()
        self.process_messages()

    def new_level(self):
        if len(self.level_maps) >= 98:
            return False

        self.level_maps.append(world.map_generator.from_template(choice(self.level_templates), self.room_templates))

        self.current_level = len(self.level_maps) - 1
        self.level_map.level = self.current_level
        color = choice(self.color_sets)
        self.level_map.colors_fov = color[0]
        self.level_map.colors_exp = color[1]

        self.global_messages.append(
            Message((19, 19), "DING! {} floor! ".format(Message.inflect(self.current_level + 1).capitalize())))

        self.spawn_test_npc()
        return True

    @property
    def level_map(self):
        return self.level_maps[self.current_level]

    def new_player(self):
        p_actor = Actor(10, 1)
        p_weapon_equipable = Equipable(None, power_bonus=11)
        p_weapon = Entity(0, 0, '/', tcod.yellow, 'Sharp Stick', 'Equipment', False,
                          description='Good for pointing and poking, but not much else',
                          component_equipable=p_weapon_equipable)

        p_equipment = Equipment(slots={'LEFT', 'RIGHT', 'FEET'})
        p_inventory = Inventory(capacity=10)
        p_viewer = Viewer(5, self.level_map.width, self.level_map.height)
        p_fighter = Fighter(10, 1, 2, 0)
        p_controlled = PlayerControlled()
        self.player_entity = Entity(19, 19, '@', tcod.white, "Player", "Player", True,
                                    render_order=RenderOrder.PLAYER,
                                    component_ai=p_controlled,
                                    component_viewer=p_viewer, component_actor=p_actor,
                                    component_fighter=p_fighter,
                                    component_equipment=p_equipment, component_inventory=p_inventory)

        self.player_entity.component_equipment.equip(p_weapon, 'LEFT')

        self.level_map.player_entity = self.player_entity
        self.level_map.entities.append(self.player_entity)
        self.player_entity.component_viewer.update_fov(self.level_map)

    def spawn_test_npc(self):
        n_equipment = Equipment(slots={'LEFT', 'RIGHT', 'FEET'})
        n_viewer = Viewer(5, self.level_map.width, self.level_map.height)
        n_fighter = Fighter(5, 1, 1, 0)
        ai_comp = FollowAI()
        npc = Entity(21, 20, '@', tcod.yellow, 'NPC', 'Player', True,
                     component_ai=ai_comp, component_viewer=n_viewer, component_fighter=n_fighter,
                     component_equipment=n_equipment)

        self.level_map.entities.append(npc)

        npc.component_viewer.update_fov(self.level_map)

    def spawn_entities(self, random_min=5, random_max=10):
        templates = {}
        for t in self.enemy_templates:
            if len(t.frequency) > 0:
                templates[t.name] = t

        pick_list = []
        for t in self.enemy_templates:
            if len(t.frequency) == 0:
                continue
            freq = t.frequency.get(max(filter(lambda f: f <= self.current_level, t.frequency)), 0)
            if freq > 0:
                pick_list += [t] * freq

        for e in range(randint(random_min, random_max + 1)):
            e = from_template(choice(pick_list), self.level_maps[self.current_level])
            self.level_map.entities.append(e)

    def spawn_boss_entities(self):
        templates = {}
        for t in self.enemy_templates:
            if len(t.treasure_room_frequency) > 0:
                templates[t.name] = t

        pick_list = []
        for t in self.enemy_templates:
            if len(t.treasure_room_frequency) == 0:
                continue
            freq = t.treasure_room_frequency.get(
                max(filter(lambda f: f <= self.current_level, t.treasure_room_frequency)), 0)
            if freq > 0:
                pick_list += [t] * freq

        for room in self.level_map.special_rooms:
            e = from_template(choice(pick_list), self.level_maps[self.current_level], room)
            self.level_map.entities.append(e)

    def spawn_treasure(self):
        # TODO: Update treasure spawining.   Random, Monster-equipment, Treasure-Rooms
        # Monster-treasure:  Generate n items for each monster from that monster's list and add to monster inventory

        templates = {}
        for t in self.random_treasure_templates:
            if len(t.frequency) > 0:
                templates[t.name] = t

        pick_list = []
        for t in self.random_treasure_templates:
            if len(t.frequency) == 0:
                continue
            freq = t.frequency.get(max(filter(lambda f: f <= self.current_level, t.frequency)), 0)
            if freq > 0:
                pick_list += [t] * freq

        for e in range(randint(10, 30 + 1)):
            e = treasure_template(choice(pick_list), self.level_maps[self.current_level])
            self.level_map.entities.append(e)

    def process_turn(self):
        self.global_messages.extend(self.level_map.take_turn())
        result = self.process_messages()
        if result == 'FLOOR_UP':
            if self.new_level():
                self.level_map.player_entity = None
                self.level_map.player_entity = self.player_entity
                self.level_map.entities.append(self.player_entity)
                self.spawn_entities()
                self.process_messages()
        elif result in ['CANCEL', 'DROP', 'USE', 'EQUIP', 'INVENTORY']:
            return result

    def process_messages(self):
        result = None
        for message in self.global_messages:
            if message.effect in ['FLOOR_UP', 'CANCEL', 'DROP', 'USE', 'EQUIP', 'INVENTORY']:
                result = message.effect
            for e in self.level_map.entities:
                if e.component_viewer and e.component_viewer.fov[message.pos[0], message.pos[1]]:
                    e.messages.append(message)
        self.global_messages = []
        return result
