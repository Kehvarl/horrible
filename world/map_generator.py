from random import choice

import numpy as np
import tcod.bsp

from loaders.level_templates import Layout
from world.level_map import LevelMap
from world.room import Room


def from_template(template: Layout, room_templates: list):
    game_map = LevelMap(template.w, template.h)

    for section in template.sections:
        s = template.sections[section]

        if s.generator and s.generator == "bsp":
            generate_bsp_area(game_map, s.x, s.y, s.w, s.h)

        elif s.generator and s.generator == "template":
            if s.room_template:
                temp = room_templates[int(s.room_template)]
            else:
                temp = choice(room_templates)

            for room_y in range(len(temp)):
                for room_x in range(len(temp[0])):
                    if temp[room_y][room_x] == '.':
                        dig(game_map, room_x + s.x, room_y + s.y)
                    elif temp[room_y][room_x] == 'E':
                        dig(game_map, room_x + s.x, room_y + s.y)
                        game_map.tiles[room_x + s.x, room_y + s.y] = 'E'

            room = Room(s.x, s.y, len(temp[0]), len(temp))
            game_map.rooms.append(room)
            game_map.start_room = room

        else:
            if s.copy in template.sections:
                ox, oy, ow, oh = template.sections[s.copy].get_area()
                clone_area(game_map, ox, oy, s.x, s.y, s.w, s.h, s.transform)

    game_map.find_special_rooms()
    return game_map


def generate_bsp_area(level_map, x, y, width, height):
    bsp = tcod.bsp.BSP(x, y, width, height)
    bsp.split_recursive(5, 5, 5, 1.0, 1.0)

    vector = []
    for room in bsp.inverted_level_order():
        if not room.children:
            vector.append((room.x + room.w // 2, room.y + room.h // 2))
            level_map.rooms.append(Room(room.x, room.y, room.w, room.h))
            for x in range(room.x + 1, room.x + room.width):
                for y in range(room.y, room.y + room.height - 1):
                    if 0 < x < level_map.width - 1 and \
                            0 < y < level_map.height - 1:
                        dig(level_map, x, y)

    # dig tunnels from opposite nodes to partition line center
    for node in bsp.inverted_level_order():
        # Skip actual rooms
        if not node.children:
            continue
        # Get the center of this area's division.
        if node.horizontal:
            partition_center = (node.x + node.w // 2, node.position)
        else:
            partition_center = (node.position, node.y + node.h // 2)

        dist_pairs = []
        for node_center in vector:
            # Get the distance from every room to the center of the dividing line
            dist = (partition_center[0] - node_center[0]) ** 2 + \
                   (partition_center[1] - node_center[1]) ** 2
            dist_pairs.append((dist, node_center))

        # get the closest 2 rooms to the dividing line
        dist_pairs.sort(key=lambda d: d[0], reverse=True)
        node1 = dist_pairs.pop()
        node2 = dist_pairs.pop()
        if node.horizontal:
            while (node1[1][1] < partition_center[1]) == \
                    (node2[1][1] < partition_center[1]):
                node2 = dist_pairs.pop()
        else:
            while (node1[1][0] < partition_center[0]) == \
                    (node2[1][0] < partition_center[0]):
                node2 = dist_pairs.pop()
        dig_tunnel(level_map, *node1[1], *node2[1])
        # self._dig_tunnel(*node2[1], *partition_center)


def clone_area(level_map, original_x, original_y, new_x, new_y, width, height, transform=None):
    b = np.copy(level_map.tiles[original_x:width, original_y:height])

    if transform == "mirror":
        b = np.flipud(b)

    if transform == "mirror2":
        b = np.fliplr(b)

    if transform == "rotate":
        b = np.flipud(b)
        b = np.fliplr(b)

    for y in range(height):
        for x in range(width):
            if b[x, y] == '.':
                dig(level_map, x + new_x, y + new_y)

    for y in range(new_y, new_y + height):
        for x in range(new_x, new_x + width):
            if level_map.walkable[x, y] and level_map.walkable[x + 1, y] and level_map.walkable[x, y + 1] \
                    and not level_map.room_containing_point(x, y):
                sx = x
                sy = y
                dx = 0
                dy = 0
                while level_map.walkable[x + dx, y]:
                    dx += 1
                while level_map.walkable[x, y + dy]:
                    dy += 1
                room = Room(sx, sy, dx + 1, dy + 1)
                level_map.rooms.append(room)


def dig_tunnel(level_map, x1, y1, x2, y2):
    xy = []
    y = min(y1, y2)
    for x in range(min(x1, x2), max(x1, x2)):
        if level_map.tiles[x, y] == '#':
            xy.append((x, y))
    x = max(x1, x2)
    for y in range(min(y1, y2), max(y1, y2)):
        if level_map.tiles[x, y] == '#':
            xy.append((x, y))

    yx = []
    x = min(x1, x2)
    for y in range(min(y1, y2), max(y1, y2)):
        if level_map.tiles[x, y] == '#':
            yx.append((x, y))
    y = max(y1, y2)
    for x in range(min(x1, x2), max(x1, x2)):
        if level_map.tiles[x, y] == '#':
            yx.append((x, y))

    if len(xy) < len(yx):
        for to_dig in xy:
            dig(level_map, to_dig[0], to_dig[1])
    else:
        for to_dig in yx:
            dig(level_map, to_dig[0], to_dig[1])


def dig(level_map, x, y):
    level_map.tiles[x, y] = '.'
    level_map.walkable[x, y] = True
    level_map.transparent[x, y] = True
