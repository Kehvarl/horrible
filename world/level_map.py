import math
from random import choice, randint

import numpy as np
import tcod.bsp
import tcod.map
import tcod.path

from entities.entity import Entity


class LevelMap(tcod.map.Map):
    def __init__(self, width, height, level=1):

        self.tile_set = {'#': 219,
                         '.': 219,
                         'E': ord('E'),
                         ' ': 219
                         }

        self.colors_fov = {'#': ((*tcod.black, 255), (*tcod.amber, 255)),
                           '.': ((*tcod.black, 255), (*tcod.black, 255)),
                           'E': ((*tcod.amber, 255), (*tcod.black, 255)),
                           ' ': ((*tcod.black, 255), (*tcod.black, 255))
                           }

        self.colors_exp = {'#': ((*tcod.black, 255), (*tcod.dark_amber, 255)),
                           '.': ((*tcod.darkest_grey, 255), (*tcod.darkest_grey, 255)),
                           'E': ((*tcod.dark_amber, 255), (*tcod.darkest_grey, 255)),
                           ' ': ((*tcod.black, 255), (*tcod.black, 255))
                           }

        self.level = level
        self.width = width
        self.height = height
        super().__init__(self.width, self.height, 'F')

        self.start_room = None
        self.rooms = []
        self.special_rooms = set()
        self.entities = []
        self.player_entity = None

        # noinspection PyTypeChecker
        self.tiles = np.full((self.width, self.height), '#', order='F')
        self.explored = np.full((self.width, self.height), False, order='F')

    def random_point(self, explored=False):
        ii = tuple(zip(*np.nonzero(self.explored == explored)))
        if len(ii) == 0:
            ii = tuple(zip(*np.nonzero(self.explored != explored)))
        return choice(ii)

    def random_room(self):
        return choice(self.rooms)

    def room_containing_point(self, x, y):
        for room in self.rooms:
            if room.is_point_in_room(x, y):
                return room
        return None

    def find_special_rooms(self, min_rooms=1, max_rooms=3):
        path = tcod.path.AStar(self.walkable)
        sx, sy = self.start_room.center
        distance = {}
        for room in self.rooms:
            rx, ry = room.center
            room_dist = len(path.get_path(sx, sy, rx, ry))
            if not distance.get(room_dist):
                distance[room_dist] = []
            distance[room_dist].append(room)

        farthest_rooms = []
        farthest_count = 10
        for farthest in sorted(distance.keys(), reverse=True):
            for room in distance[farthest]:
                if farthest_count > 0:
                    farthest_rooms.append(room)
                    farthest_count -= 1
                else:
                    break

        special_rooms_count = randint(min_rooms, max_rooms)
        while len(self.special_rooms) < special_rooms_count:
            sc = choice(farthest_rooms)
            if sc not in self.special_rooms:
                self.special_rooms.add(sc)

    def adjacent_entities(self, entity):
        ex = entity.x
        ey = entity.y
        neighbors = [(ex - 1, ey), (ex + 1, ey), (ex, ey - 1), (ex, ey + 1)]
        adj = filter(lambda e: (e.x, e.y) in neighbors, self.entities)
        return adj

    def entities_in_room(self, room, entity=None):
        in_room = set()
        for e in self.entities:
            if e != entity and (e.x, e.y) in room.all_points_in_room():
                in_room.add(e)
        return in_room

    def get_entity(self, entity=None, faction=None, exclude=None):
        working_list = list(filter(lambda e: (faction is None or e.faction in faction) and
                                             (exclude is None or e.faction not in exclude) and
                                             (e != entity), self.entities))

        return choice(working_list)

    def get_entities_at_location(self, entity_x, entity_y):

        result = []
        for entity in sorted(self.entities, key=lambda e: e.render_order.value, reverse=True):
            if entity.x == entity_x and entity.y == entity_y:
                result.append(entity)

        return result

    def get_blocking_entities_at_location(self, destination_x, destination_y):
        """
        Return the first Entity which blocks movement at a given x,y coordinate.
        :param int destination_x:
        :param int destination_y:
        :return: The found Entity or None
        :rtype: Entity
        """
        for entity in self.entities:
            if entity.blocks and entity.x == destination_x and entity.y == destination_y:
                return entity

        return None

    def take_turn(self):
        messages = []
        for entity in self.entities:
            # if entity == self.player_entity:
            #     continue
            result = entity.take_turn(self)
            while result is False:
                result = entity.take_turn(self)
            messages.extend(result)
        return messages

    def move(self, entity, dx=0, dy=0):
        if entity not in self.entities:
            return False
        entity = self.entities[self.entities.index(entity)]

        nx = entity.x + dx
        ny = entity.y + dy

        if self.walkable[nx, ny]:
            entity.x = nx
            entity.y = ny

        if entity.component_viewer:
            entity.component_viewer.update_fov(self)

    def move_towards(self, entity, target_x, target_y):
        dx = target_x - entity.x
        dy = target_y - entity.y
        distance = math.sqrt(dx ** 2 + dy ** 2)
        if distance > 0:
            dx = int(round(dx / distance))
            dy = int(round(dy / distance))

        if (self.walkable[entity.x + dx, entity.y + dy] and
                not self.get_blocking_entities_at_location(entity.x + dx, entity.y + dy)):
            self.move(entity, dx, dy)

    def __repr__(self):
        output = ""
        for map_x in range(self.width):
            line = ""
            for map_y in range(self.height):
                line += self.tiles[map_x, map_y]
            output += line + "\n"
        return output

    def fill_buffer(self, con, level, no_fog=False, confusion=False, ):
        fov = self.player_entity.component_viewer.fov
        explored = self.player_entity.component_viewer.explored
        for x in range(self.width):
            for y in range(self.height):
                if fov[x, y]:
                    con.buffer[x, y] = (self.tile_set[self.tiles[x, y]],
                                        *self.colors_fov[self.tiles[x, y]])
                elif explored[level, x, y] or no_fog:
                    con.buffer[x, y] = (self.tile_set[self.tiles[x, y]],
                                        *self.colors_exp[self.tiles[x, y]])
                else:
                    con.buffer[x, y] = (self.tile_set[' '],
                                        *self.colors_fov[' '])

        for entity in sorted(self.entities, key=lambda e: e.render_order.value, reverse=True):
            if fov[entity.x, entity.y] or no_fog:
                con.buffer[entity.x, entity.y] = (ord(entity.symbol),
                                                  (*entity.color, 255),
                                                  (*tcod.black, 255))

        if confusion:
            for pocket in range(randint(3, 9)):
                x, y, = self.random_point(False)
                x1, y1 = self.random_point(True)
                con.buffer[x, y] = (self.tile_set[self.tiles[x1, y1]],
                                    *self.colors_fov[self.tiles[x1, y1]])
