from random import randint


class Room:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self._room_points = None

    @property
    def w(self):
        return self.width

    @property
    def h(self):
        return self.height

    @property
    def center(self):
        return (self.x + self.width) // 2, (self.y + self.height) // 2

    def is_point_in_room(self, x, y):
        return self.x <= x <= (self.x + self.width) and self.y <= y <= (self.y + self.height)

    def random_point_in_room(self):
        """
        Returns a random point somewhere within the selected room
        """
        x = randint(1, self.width - 2) + self.x
        y = randint(1, self.height - 2) + self.y
        return x, y

    def all_points_in_room(self):
        if not self._room_points:
            self._room_points = [(x, y) for x in range(self.x, self.x + self.width)
                                 for y in range(self.y, self.y + self.height)]
        return self._room_points

    def __repr__(self):
        return "({},{}): {}x{}".format(self.x, self.y, self.w, self.h)
