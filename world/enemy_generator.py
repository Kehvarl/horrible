from entities.components.actor import Actor
from entities.components.ai import *
from entities.components.equipable import Equipable
from entities.components.fighter import Fighter
from entities.components.treasure import Treasure
from entities.components.viewer import Viewer
from entities.entity import Entity
from loaders.enemy_templates import Enemy


def from_template(template: Enemy, level_map, room=None):
    actor_component = None
    fighter_component = None
    viewer_component = None
    equipment_component = None
    treasure_component = None
    ai_component = None
    if room is None:
        room = level_map.random_room()
    x, y = room.random_point_in_room()

    if "actor" in template.components:
        eng, gain = template.components["actor"]
        actor_component = Actor(eng, gain)

    if "detector" in template.components:
        radius = template.components["detector"][0]
        viewer_component = Viewer(radius, level_map.width, level_map.height)

    if "fighter" in template.components:
        hp, dmg, defense, xp = template.components["fighter"]
        fighter_component = Fighter(int(hp), int(dmg), int(defense), int(xp))

    if "equipment" in template.components:
        equipment_component = Equipable()

    if "treasure" in template.components:
        value, xp = template.components["treasure"]
        treasure_component = Treasure(int(value), int(xp))

    if "ai" in template.components:
        ai_type = template.components["ai"]
        if ai_type == "Guardian":
            room = level_map.random_room()
            x, y = room.random_point_in_room()
            ai_component = GuardianAI(room=room)
        elif ai_type == "Follower":
            ai_component = FollowAI()
        else:
            ai_component = BasicAI()

    return Entity(x, y, template.symbol, template.color, template.name, template.faction, True,
                  description=template.description,
                  component_ai=ai_component, component_actor=actor_component,
                  component_equipment=equipment_component,
                  component_treasure=treasure_component,
                  component_fighter=fighter_component, component_viewer=viewer_component)
