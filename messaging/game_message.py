import tcod


class MessageList:
    def __init__(self, max_lines=8):
        self.messages = []
        self.max_lines = max_lines

    def append(self, message):
        self.messages.append(message)
        if len(self.messages) > self.max_lines:
            self.messages = self.messages[(len(self.messages) - self.max_lines):]

    def __iter__(self):
        return self.messages.__iter__()


class Message:
    inflections = {
        0: "ground", 1: "first", 2: "second", 3: "third", 4: "fourth", 5: "fifth", 6: "sixth", 7: "seventh",
        8: "eighth", 9: "ninth", 10: "tenth", 11: "eleventh", 12: "twelfth", 13: "thirteenth", 14: "fourteenth",
        15: "fifteenth", 16: "sixteenth", 17: "seventeenth", 18: "eighteenth", 19: "nineteenth",
        20: ["twentieth", "twenty"], 30: ["thirtieth", "thirty"], 40: ["fortieth", "forty"],
        50: ["fiftieth", "fifty"], 60: ["sixtieth", "sixty"], 70: ["seventieth", "seventy"],
        80: ["eightieth", "eighty"], 90: ["ninetieth", "ninety"],
    }

    def __init__(self, pos, text, color=tcod.white, effect=None):
        """

        :param text:
        :param color:
        """
        self.pos = pos
        self.text = text
        self.color = color
        self.effect = effect

    @staticmethod
    def inflect(number):
        if number in Message.inflections:
            return Message._get_inflect(number)
        else:
            output = ""
            tens = (number // 10) * 10
            ones = number - tens
            if tens in Message.inflections:
                output += Message._get_inflect(tens, 1) + " "
            if ones in Message.inflections:
                output += Message._get_inflect(ones)
            return output.strip()

    @staticmethod
    def _get_inflect(number, pos=0):
        if isinstance(Message.inflections[number], list):
            return Message.inflections[number][pos]
        else:
            return Message.inflections[number]


if __name__ == "__main__":
    for n in range(100):
        print(Message.inflect(n))
