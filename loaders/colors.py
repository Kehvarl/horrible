import tcod
import toml


class Color:

    @staticmethod
    def load_all_from_toml_file(toml_file_path):
        color_sets = []
        t = toml.load(toml_file_path)

        for color_set in t.get("colors"):
            col = t.get("colors")[color_set]
            colors_fov = {}
            for fov in col['colors_fov']:
                # fov_def = col['colors_fov'][fov]
                identifier = fov.get("identifier", ' ')
                fg = (*getattr(tcod, fov.get("fg"), "black"), 255)
                bg = (*getattr(tcod, fov.get("bg"), "white"), 255)
                colors_fov[identifier] = (fg, bg)
            colors_exp = {}
            for exp in col['colors_exp']:
                # exp_def = col['colors_fov'][exp]
                identifier = exp.get("identifier", ' ')
                fg = (*getattr(tcod, exp.get("fg"), "black"), 255)
                bg = (*getattr(tcod, exp.get("bg"), "white"), 255)
                colors_exp[identifier] = (fg, bg)

            color_sets.append((colors_fov, colors_exp))

        return color_sets
