import tcod
import toml


class Treasure:
    def __init__(self):
        self.frequency = {}
        self.treasure_room_frequency = {}
        self.level_max = 0
        self.name = "Default"
        self.symbol = 'd'
        self.description = None
        self.color = tcod.red
        self.special_effects = set()
        self.duration = 0
        self.components = {}

    @staticmethod
    def load_all_from_toml_file(toml_file_path):
        treasures = []
        t = toml.load(toml_file_path)

        for entity_name in t:
            treasure = Treasure()
            tr_tmp = t[entity_name]

            treasure.level_max = tr_tmp.get("max_per_level", 0)
            treasure.name = tr_tmp.get("name", "Default")
            treasure.symbol = tr_tmp.get("symbol", "d")
            treasure.description = tr_tmp.get("description", None)
            treasure.color = getattr(tcod, tr_tmp.get("color", "red"), tcod.red)

            for key in tr_tmp.get("frequency", []):
                treasure.frequency[int(key)] = tr_tmp.get("frequency", {0: 1})[key]

            for key in tr_tmp.get("treasure_room_frequency", []):
                treasure.treasure_room_frequency[int(key)] = tr_tmp.get("treasure_room_frequency", {0: 1})[key]

            for effect in tr_tmp.get("special_effects", []):
                treasure.special_effects.add(effect)

            if tr_tmp.get("treasure"):
                treasure.components["treasure"] = (tr_tmp['treasure_value'], tr_tmp['treasure_xp'])

            if tr_tmp.get("equipable"):
                treasure.components["equipable"] = (tr_tmp['equip_slot'], tr_tmp['equip_max_hp'],
                                                    tr_tmp['equip_power'], tr_tmp['equip_defense'])

            if tr_tmp.get("consumable"):
                consume_special = []
                for effect in tr_tmp.get("consume_special"):
                    consume_special.append(effect)
                treasure.components["consumable"] = (tr_tmp['consume_hp'], tr_tmp['consume_power'],
                                                     tr_tmp['consume_defense'], tr_tmp['consume_duration'],
                                                     consume_special)

            treasures.append(treasure)

        return treasures
