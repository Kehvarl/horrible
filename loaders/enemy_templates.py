import os

import tcod
import toml

from loaders.treasure_templates import Treasure


class Enemy:
    def __init__(self):
        self.frequency = {}
        self.treasure_room_frequency = {}
        self.level_max = 0
        self.faction = "Default"
        self.allied_factions = set()
        self.name = "Default"
        self.description = None
        self.symbol = 'd'
        self.color = tcod.red
        self.special_effects = set()
        self.components = {}
        self.treasure_templates = {}

    @staticmethod
    def load_all_from_toml_file(toml_file_path):
        enemies = []
        t = toml.load(toml_file_path)

        for entity_name in t:
            enemy = Enemy()
            e_tmp = t[entity_name]

            enemy.level_max = e_tmp.get("max_per_level", 0)
            enemy.faction = e_tmp.get("faction", "Default")
            enemy.name = e_tmp.get("name", "Default")
            enemy.description = e_tmp.get("description", None)
            enemy.symbol = e_tmp.get("symbol", "d")
            enemy.color = getattr(tcod, e_tmp.get("color", "red"), tcod.red)

            for key in e_tmp.get("frequency", []):
                enemy.frequency[int(key)] = e_tmp.get("frequency", {0: 1})[key]

            for key in e_tmp.get("treasure_room_frequency", []):
                enemy.treasure_room_frequency[int(key)] = e_tmp.get("treasure_room_frequency", {0: 1})[key]

            for ally in e_tmp.get("allies"):
                enemy.allied_factions.add(ally)

            for effect in e_tmp.get("special_effects", []):
                enemy.special_effects.add(effect)

            if e_tmp.get("actor"):
                enemy.components["actor"] = (e_tmp['energy'], e_tmp['energy_gain'])

            if e_tmp.get("detector"):
                enemy.components["detector"] = (e_tmp['visible_range'],)

            if e_tmp.get("fighter"):
                enemy.components["fighter"] = (e_tmp['hp'], e_tmp['dmg'], e_tmp['defense'], e_tmp['xp'])

            if e_tmp.get("treasure"):
                enemy.components["treasure"] = (e_tmp['treasure_value'], e_tmp['treasure_xp'])

            if e_tmp.get("ai"):
                enemy.components["ai"] = (['e_tmp.ai_type'],)

            treasure_file = "resources/enemy_treasure/{}.toml".format(enemy.name)
            if os.path.isfile(treasure_file):
                enemy.treasure_templates = Treasure.load_all_from_toml_file(treasure_file)

            enemies.append(enemy)

        return enemies
