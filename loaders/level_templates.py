import toml


class Section:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.w = 1
        self.h = 1
        self.name = "UnNamed Layout"
        self.generator = False
        self.room_template = False
        self.copy = False
        self.transform = False

    def get_area(self):
        return self.x, self.y, self.w, self.h


class Layout:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.w = 1
        self.h = 1
        self.name = "UnNamed Layout"
        self.sections = {}

    @staticmethod
    def load_all_from_toml_file(toml_file_path):
        templates = []
        t = toml.load(toml_file_path)

        for layout_name in t.get("templates"):
            working_template = t.get("templates").get(layout_name)
            new_layout = Layout()
            new_layout.name = layout_name
            new_layout.sections = {}
            new_layout.w = working_template.get("width", 1)
            new_layout.h = working_template.get("height", 1)
            for section_template in working_template['sections']:
                section = Section()
                section.x = section_template.get("x", 0)
                section.y = section_template.get("y", 0)
                section.w = section_template.get("w", 1)
                section.h = section_template.get("h", 1)
                section.name = section_template.get("name", "ZZ")
                section.generator = section_template.get("generator")
                section.room_template = section_template.get("template")
                section.copy = section_template.get("copy")
                section.transform = section_template.get("transformation")
                new_layout.sections[section.name] = section

            templates.append(new_layout)
        return templates
