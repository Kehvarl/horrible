from game_states import GameStates
from ui.menus.equipmentmenu import EquipmentMenu, EquipmentCommands
from ui.menus.inventorymenu import InventoryMenu, InventoryCommands
from ui.menus.mainmenu import MainMenu
from ui.ui import UI
from world.game_world import World

game_title = 'Horrible Hirise'

game = World()

gui = UI(game, game_title)
main_menu = MainMenu(background_image="resources/horriblehirise2.png",
                     options_title='Main Menu',
                     options=[('NEW', 'Start New Game'), ('QUIT', 'Quit Game')])

item_menu = None
equipment_menu = None

game_state = GameStates.MAIN_MENU

# Main Game Loop
while True:
    if game_state in [GameStates.TURN, GameStates.PLAYERS_TURN]:
        gui.render(game)
        command = game.process_turn()
        if command in ['DROP', 'USE', 'INVENTORY']:
            game_state = GameStates.INVENTORY_MENU
            item_menu = InventoryMenu(options=game.player_entity.component_inventory.stored_items_options,
                                      options_title='Available Inventory')

        if command in ['EQUIP', 'UNEQUIP']:
            game_state = GameStates.EQUIPMENT_MENU
            equipment_menu = EquipmentMenu(options_title="Slots",
                                           item=None,
                                           slots=game.player_entity.component_equipment.slot_items)

    elif game_state == GameStates.INVENTORY_MENU:
        gui.render_menu(item_menu)
        response = item_menu.process()
        if response:
            command, item = response
            if command == InventoryCommands.CANCEL:
                game_state = GameStates.TURN
            elif command == InventoryCommands.DROP and item is not None:
                game.player_entity.component_inventory.drop(item, game.level_map)
                game_state = GameStates.TURN

    elif game_state == GameStates.EQUIPMENT_MENU:
        game_state = GameStates.TURN
        response = item_menu.process()
        if response:
            command, item = response
            if command == EquipmentCommands.CANCEL:
                game_state = GameStates.TURN

    elif game_state == GameStates.MAIN_MENU:
        gui.render_menu(main_menu)
        response = main_menu.process()
        if response == 'NEW':
            game_state = GameStates.TURN
        elif response == 'QUIT':
            exit()
